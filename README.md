# Aplicaciones móviles 3D y sensores inteligentes

## Resumen
Se realizó una ampliación a una aplicación móvil 3D de domótica programada en Unity de la Facultad de Infomatica, Universidad de La Plata.
Mediante una solución integral, la aplicación permite  el control automático de los dispositivos físicos de un espacio multifuncional (oficina, casa, etc) de manera sencilla y a partir de un dispositivo móvil.

### Tareas Realizadas 
Se realizó la adición de dos nuevos formatos de vista al “Modo Utilización” para que el usuario pueda 
elegir entre varias alternativas de visualización y control de los dispositivos

#### Primera Persona

Para que el usuario pueda sentir que se encuentra en la habitación de manera más realista se 
implementó el formato de cámaras en Primera Persona con un control de mando (joystick) para que 
el usuario pueda desplazarse con mayor comodidad por la habitación creada.

![](1.PNG)

En relación a los controles de mando, el joystick derecho sirve para el movimiento de la cámara 
mientras que el joystick de la izquierda sirve para el desplazamiento del usuario en la habitación.

#### Realidad Virtual

El segundo modo de visualización que fue agregado es para que el usuario pueda sentir una mayor 
inmersión dentro de los ambientes virtuales. Este modo incluye funcionalidad de Realidad Virtual, y 
debe ser utilizado junto a unas gafas de RV.

![](2.PNG)

En este modo de uso, el usuario podrá recorrer los diferentes ambientes utilizando unas gafas de 
realidad virtual. Para moverse, deberá mirar levemente hacia abajo y el personaje comenzará a 
caminar. Para cambiar a la habitación siguiente, el usuario deberá caminar hasta el “chocarse” con 
el extremo derecho de la habitación actual y quedarse sin moverse por 3 segundos. Análogamente, 
para volver a la habitación anterior deberá realizar la misma secuencia de acciones, pero 
colocándose en el extremo izquierdo.

#### Comunicación con Cloud

Se realizó la migración del servidor de la nube Amazon cloud a otro servidor de la nube: Google 
cloud. En la instancia se instaló un sistema operativo de software libre y código abierto (Ubuntu) 
,luego se instaló Mosquitto para que trabaje como agente de mensajes usando el protocolo MQTT.

![](3.png)


## Autores

AUTORES: Jeronimo Alfonsin, Antonella Palladino Hours, Mauro Santos  

DIRECTORES: Sebastián Dapoto, Diego Encinas, Federico Cristina

## Estado del Proyecto

El proyecto temporalmente se avanzara un poco más y estara terminado.

